<?php

session_start();
if(isset($_SESSION["id"])){
  $id=intval($_SESSION["id"]);
}
else{
  $id=0;
}

//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "pida_db";

$conn = new mysqli($servername,$username,$password,$database);
?>

 <!DOCTYPE html>
 <html lang="it">
 <head>
 	<title>Pida Admin Page</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="css/bootstrap.css" rel="stylesheet">
   <link rel="stylesheet" href="css/custom-style.css">
   <link rel="stylesheet" href="css/payment.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <script src="js/custom-js.js"></script>
  </head>
 <body>
 <!-- Fixed navbar -->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <ul class="nav navbar-nav navbar-left">
          <li><a class="navbar-brand no-padding" href="index.php"><img src="images/logo.png" alt="" id="logo"></a></li>
          <li>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar top-bar"></span>
              <span class="icon-bar bottom-bar"></span>
            </button>
          </li>
        </ul>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right center-mobile mobile-nav">
          <li class="shadow-top"><a href="index.php#proposte">Le nostre proposte</a></li>
          <li class="shadow-top"><a href="index.php#chisiamo">Chi siamo</a></li>
          <li class="shadow-top"><a href="index.php#dovesiamo">Dove siamo</a></li>
          <?php
          if($id=='0') {
            ?>
          <li class="shadow-top"><a href="accedi_registrati.php">Accedi / Registrati</a></li>
          <?php
        } else { ?>
            <li class="shadow-top"><a href="index.php?logout=true">Logout</a></li>
            <?php }
            ?>
            <li class="shadow-top"><a href="admin.php">Admin</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>









  <div class="line">
    <br><br>
    <h1 class="description" style="text-align:center">Aggiungi una nuova piadina al menù</h1>
  </div>
  <section class="col col-centered col-xs-12 col-sm-6 col-md-3 col-lg-2">
    <form action="admin.php" method="post">
      <div class="form-group">
        <label for="nome">Nome
          <input type="text" class="form-control-ar" placeholder="Es. Classica" id="nome" name="nome">
          <?php
            if( isset( $_POST['nome'] ) && !empty( $_POST['nome'] )){
              $nome = $_POST['nome'];
            }
          ?>
        </label>
      </div>
      <div class="form-group">
        <label for="descrizione">Descrizione
          <input type="text" class="form-control-ar" placeholder="Es. Pomodoro" id="descrizione" name="descrizione">
            <?php
              if( isset( $_POST['descrizione'] ) && !empty( $_POST['descrizione'] )){
                $descrizione = $_POST['descrizione'];
              }
            ?>
          </input>
        </label>
      </div>
      <div class="form-group">
        <label for="prezzo">Prezzo
          <input type="number" class="form-control-ar" placeholder="Es. 3.5" step="0.01" id="prezzo" name="prezzo">
          <?php
            if( isset( $_POST['prezzo'] ) && !empty( $_POST['prezzo'] )){
              $prezzo = $_POST['prezzo'];
            }
          ?>
        </label>
      </div>
      <div class="form-group">
        <label for="submit">
          <input type="submit" class="btn btn-default" id="submit">
          <?php


            $sql = "SELECT MIN(num) AS num FROM listino WHERE num>0";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) {
                $minimo=$row["num"];
              }
              if($minimo>15){
                $message = "Hai aggiunto il numero massimo di piadine";
                echo "<script type='text/javascript'>alert('$message');</script>";
              }
              $minimo--;
              if($minimo==0){
                $minimo=1;
              }
            } else {
              $minimo = 1;
            }

            $flag=true;
            while($flag){
              $sql = "SELECT nome AS nome FROM listino WHERE num=$minimo";
              $result = $conn->query($sql);
              $row = $result->fetch_assoc();
              if($row==NULL){
                if( isset( $nome ) && !empty( $nome) &&
                  isset( $descrizione ) && !empty( $descrizione ) &&
                  isset( $prezzo ) && !empty( $prezzo )){
                  $sql = "INSERT INTO listino (num, nome, descrizione, prezzo) VALUE ('$minimo','$nome','$descrizione','$prezzo')";
                  $result = $conn->query($sql);
                  header("Refresh:0");
                }
                $flag=false;
              } else {
                $minimo++;
                if($minimo>15){
                  $message = "Hai aggiunto il numero massimo di piadine";
                  echo "<script type='text/javascript'>alert('$message');</script>";
                  $flag=false;
                }
              }

            }
          ?>
        </label>
      </div>
    </form>
    </section>




    <div class="line">
      <br>
      <h1 class="description" style="text-align:center">Rimuovi una piadina dal menù</h1>
    </div>



    <section class="col col-centered col-xs-12 col-sm-6 col-md-3 col-lg-2">

        <div class="form-group">
          <label for="delnum">Numero piadina da cancellare
          <input type="number" class="form-control-ar" placeholder="Es. 1" id="delnum" name="delnum">
          <?php
            if( isset( $_POST['delnum'] ) && !empty( $_POST['delnum'] )) {
              $delnum = $_POST['delnum'];
            }
          ?>
          </label>
        </div>

        <div class="form-group">
          <label for="submit">
            <input type="submit" class="btn btn-default" id="submit">
            <?php
            if( isset( $delnum ) && !empty( $delnum )){
              $sql = "DELETE FROM listino WHERE num='$delnum'";
              $result = $conn->query($sql);
              $result=null; $sql=null;
            }
            ?>
          </label>
        </div>
 	</section>




  <?php
  $sql = "SELECT * FROM listino";
  $result = $conn->query($sql);
  ?>

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th scope="col">Numero</th>
                <th scope="col">Nome</th>
                <th scope="col">Descrizione</th>
                <th scope="col">Prezzo</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                  ?>
                    <tr>
                      <td><?php echo $row["num"]; ?></td>
                      <td><?php echo $row["nome"]; ?></td>
                      <td><?php echo $row["descrizione"]; ?></td>
                      <td><?php echo $row["prezzo"]; ?></td>
                    </tr>
                  <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div><!--end of .table-responsive-->
      </div>
    </div>
  </div>

  <footer>
    <div class="footer" id="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
            <h3> Chi siamo </h3>
            <ul>
              <li> <a href="#"> Termini e Condizioni </a> </li>
              <li> <a href="#"> Cookie Policy </a> </li>
              <li> <a href="#dovesiamo"> Lavora con noi </a> </li>
            </ul>
          </div>
          <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
            <h3> Contattaci </h3>
            <ul>
              <li> Tel: 0547 123456</li>
              <li> Email: pidatogo@info.it</li>
              <li> Cellulare: 333 123456</li>
            </ul>
          </div>
          <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12 ">
            <h3> Trovaci sui social </h3>
            <p>Qui potrai trovarci su diversi social, ti aspettiamo!</p>
            <ul class="social col-sm-12 col-xs-12">
              <li> <a href="#"> <img src="images/facebook.png" alt=""> </a> </li>
              <li> <a href="#"> <img src="images/linkedin.png" alt=""> </a> </li>
              <li> <a href="#"> <img src="images/twitter.png" alt=""> </a> </li>
              <li> <a href="#"> <img src="images/pinterest.png" alt=""> </a> </li>
              <li> <a href="#"> <img src="images/youtube.png" alt=""> </a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <p class="pull-left"> Copyright © Footer E-commerce Plugin 2014. All right reserved. </p>
        <div class="pull-right">
          <ul class="nav nav-pills payments col-sm-12 col-xs-12">
            <li><img src="images/p1.png" alt=""></li>
            <li><img src="images/p2.png" alt=""></li>
            <li><img src="images/p3.png" alt=""></li>
            <li><img src="images/p4.png" alt=""></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>
