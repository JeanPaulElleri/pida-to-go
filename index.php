<?php
session_start();
if(isset($_SESSION["id"])){
  $id=intval($_SESSION["id"]);
}
else{
  $id=0;
}

if(isset($_GET['logout'])) {
  session_unset();
  session_destroy();
  $url = "index.php";
  header( "Location: $url" );
}
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "pida_db";

$conn = new mysqli($servername,$username,$password,$database);



if(isset($_GET['numpida'])){

  if ($_GET['numpida'] == '1') {
  $sql = "UPDATE carrello SET p1=p1+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '2') {
  $sql = "UPDATE carrello SET p2=p2+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '3') {
  $sql = "UPDATE carrello SET p3=p3+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '4') {
  $sql = "UPDATE carrello SET p4=p4+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '5') {
  $sql = "UPDATE carrello SET p5=p5+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '6') {
  $sql = "UPDATE carrello SET p6=p6+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '7') {
  $sql = "UPDATE carrello SET p7=p7+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '8') {
  $sql = "UPDATE carrello SET p8=p8+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '9') {
  $sql = "UPDATE carrello SET p9=p9+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '10') {
  $sql = "UPDATE carrello SET p10=p10+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '11') {
  $sql = "UPDATE carrello SET p11=p11+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '12') {
  $sql = "UPDATE carrello SET p12=p12+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '13') {
  $sql = "UPDATE carrello SET p13=p13+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '14') {
  $sql = "UPDATE carrello SET p14=p14+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
  if ($_GET['numpida'] == '15') {
  $sql = "UPDATE carrello SET p15=p15+1 WHERE id=$id";
  $result = $conn->query($sql);
  }
}

?>

<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <title>Pida-To-Go</title>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="css/custom-style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="js/custom-js.js"></script>
</head>

<body>
  <nav id="global-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <ul class="nav navbar-nav navbar-left">
          <li><a class="navbar-brand no-padding" href=""><img src="images/logo.png" alt="Pida-To-Go" id="logo"></a></li>
          <li>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar top-bar"></span>
              <span class="icon-bar bottom-bar"></span>
            </button>
          </li>
        </ul>
      </div>
      <div id="myNavbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right center-mobile mobile-nav">
          <li class="shadow-top"><a href="#proposte">Le nostre proposte</a></li>
          <li class="shadow-top"><a href="#chisiamo">Chi siamo</a></li>
          <li class="shadow-top"><a href="#dovesiamo">Dove siamo</a></li>
          <?php
          if($id=='0') {
            ?>
          <li class="shadow-top"><a href="accedi_registrati.php">Accedi / Registrati</a></li>
          <?php
        } else { ?>
            <li class="shadow-top"><a href="index.php?logout=true">Logout</a></li>
            <?php }

            $sql = "SELECT * FROM carrello WHERE id=$id";
            $result = $conn->query($sql);
            $row = $result->fetch_assoc();

            if($row["id"]=='1'){ ?>
              <li class="shadow-top"><a href="admin.php">Admin</a></li>
            <?php } else {
              if($row["id"]!='1' && $row["p1"]== 0 && $row["p2"]== 0 && $row["p3"]== 0 && $row["p4"]== 0 && $row["p5"]== 0 &&
              $row["p6"]== 0 && $row["p7"]== 0 &&$row["p8"]== 0 && $row["p9"]== 0 &&
              $row["p10"]== 0 && $row["p11"]== 0 && $row["p12"]== 0 && $row["p13"]== 0 &&
              $row["p14"]== 0 && $row["p15"]== 0) { ?>
                <li><a href="cart.php?sendcode=false" id="cart-icon"><img src="images/cart28.png" alt="cart logo"></a></li>
              <?php } else { ?>
                <li><a href="cart.php?sendcode=false" id="cart-icon"><img src="images/cart-2.png" alt="cart logo"></a></li>
              <?php }
              }
          ?>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>

  <div class="main-img">
    <video id="video" width="1920" height="850" autoplay loop>
      <source src="images/video_copertina.webm" type="video/webm">
      <source src="images/video_copertina.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>
  </div>

  <div class="mobile-main">
     <img src="loghi/LOGO_F_ombr.png" alt="Pida-To-Go">
     <p>La Pìda cun e Parsòt, <br>la pìs un po’ ma tòt.</p>
   </div>

  <div class="line" id="proposte">
    <br><br>
      <h1 class="description">Le nostre proposte</h1>
  </div>








    <?php
      $sql = "SELECT * FROM listino";
      $result = $conn->query($sql);
    ?>

    <section class="team">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="col-lg-12">
              <div class="row pt-md">

                  <?php
                    if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                  ?>

                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 profile">
                    <div class="img-box">

                      <?php $nome = $row["nome"];
                      $num = $row["num"];
                      if($nome == "Leggera") { ?>
                        <img src= "loghi/leggera.png" class="img-responsive" alt="leggera">
                      <?php }

                      if($nome == "Montanara") { ?>
                        <img src= "loghi/montanara.png" class="img-responsive" alt="montanara">
                      <?php }

                      if($nome == "Americana") { ?>
                        <img src= "loghi/americana.png" class="img-responsive" alt="americana">
                      <?php }

                      if($nome == "Piada Burger") { ?>
                        <img src= "loghi/burger.png" class="img-responsive" alt="burger">
                      <?php }

                      if($nome == "Vegetariana") { ?>
                        <img src= "loghi/vegetariana.png" class="img-responsive" alt="vegetariana">
                      <?php }

                      if($nome == "Sfiziosa") { ?>
                        <img src= "loghi/sfiziosa.png" class="img-responsive" alt="sfiziosa">
                      <?php }

                      if($nome == "Golosa") { ?>
                        <img src= "loghi/golosa.png" class="img-responsive" alt="golosa">
                      <?php }

                      if($nome == "Tradizionale") { ?>
                        <img src= "loghi/tradizionale.png" class="img-responsive" alt="tradizionale">
                      <?php }

                          if ($nome != "Sfiziosa" &&
                          $nome != "Tradizionale" &&
                          $nome != "Vegetariana" &&
                          $nome != "Golosa" &&
                          $nome != "Piada Burger" &&
                          $nome != "Americana" &&
                          $nome != "Montanara" &&
                          $nome != "Leggera" ) { ?>
                          <img src= "loghi/generica.png" class="img-responsive" alt="generica">
                          <?php } ?>


                      <div class="pida-cart-icon">
                        <?php
                        if($id>'0'){ ?>
                          <a href='index.php?numpida=<?php echo $num ?>#proposte'><img src="images/cart.png" alt="cart"></a>
                      <?php  } else { ?>
                        <a href='#' class="cart-no-id"><img src="images/cart.png" alt="cart not empty"></a>
                      <?php } ?>

                      </div>
                    </div>


                    <h1><strong><?php echo $row["nome"]; ?></strong></h1>
                    <h4><?php echo $row["prezzo"]; ?> €</h4>
                    <p><?php echo $row["descrizione"]; ?></p>
                  </div>
                  <?php }} ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div class="line" id="chisiamo">
      <br>
      <br>

      <h1>Chi siamo?</h1>
    </div>
    <section class="container-fluid" >
        <div class="row">
          <div class="col-lg-7 col-md-10 col-sm-10 col-xs-12 col-centered">

            <div class="container col-lg-4 col-md-4 col-sm-6 col-xs-12" >
                <div class="span3 well" style="text-align:center">
                    <img src="images/selly.jpg" width="140" height="140" class="img-circle" alt="selly dev">
                    <h3>Claudia Severi</h3>
                </div>

            </div>

            <div class="container col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="span3 well" style="text-align:center">
                    <img src="images/elena.jpg" width="140" height="140" class="img-circle" alt="elena dev">
                    <h3>Elena Alessandrini</h3>
                </div>
            </div>

            <div class="container col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="span3 well" style="text-align:center">
                    <img src="images/jampy.jpg" width="140" height="140" class="img-circle" alt="jean paul dev">
                    <h3>Jean Paul Elleri</h3>
                </div>

            </div>

             <p>Pida-to-go inaugura il primo chiosco a Cesena nel gennaio 2018 con l'intenzione di espandersi in tutta la Romagna.
              Abbiamo deciso di venire incontro alle esigenze dei nostri clienti fornendo un servizio rapido e veloce di consegna sul
              nostro territorio.</p>
             <p> La politica aziendale prevede l'utilizzo di prodotti locali per favorire le piccole aziende romagnole. La ricerca della
               qualità è da sempre un punto focale alla base della nostra attività.<p>
               <strong id="dovesiamo">Perchè "Pida-to-go"?</strong>
                            Il nome Pida-To-Go nasce dal connubio tra l'amore per la piadina romagnola e l'innovazione dal punto di vista tecnologico
                          di un servizio che appartiene alla tradizione del nostro territorio da generazioni.</p>
           </div>
         </div>
       </section>

       <div class="line">
         <h1>Dove siamo?</h1>
       </div>
         <section class="container-fluid">
             <div class="row">
               <div class="col-lg-7 col-md-10 col-sm-10 col-xs-12 col-centered">

             <h4>Vieni a trovarci a <strong>Cesena</strong>, consulta la mappa per sapere dove siamo!</h4>
             <div class="row">
               <div class="col-lg-3">
                 <address>
                     <strong>Indirizzo</strong><br>
                     Via Sacchi, 3<br>
                     Cesena FC<br>
                     Tel: 0547 123456
                 </address>
               </div>
               <div class="col-lg-4">
                 <address>
                   <strong>Contattaci</strong><br>
                   <a href="mailto:infopidatogo@gmail.com">infopidatogo@gmail.com</a>
                 </address>
               </div>
               <div class="col-lg-5">
                 <address>
                   <strong>Vorresti il servizio Pida-to-go nella tua città?</strong><br>
                   Contattaci ad uno dei nostri recapiti e saremo lieti di ascoltarti!
                 </address>
               </div>
             </div>
           </div>
        </div>
      </section>

      <div id="map"></div>
        <script>
        function myMap() {
          var mapCanvas = document.getElementById("map");
          var mapOptions = {
            center: new google.maps.LatLng(44.15, 12.1), zoom: 10
          };
          var map = new google.maps.Map(mapCanvas, mapOptions);
          var loc={lat: 44.142153, lng: 12.238278};
          var marker = new google.maps.Marker({
          position: loc,
          map: map
        });
        }
        </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq9-D5Nrx09zAX-T_XuMlTlFgFyNOFJpo&callback=myMap"></script>









      <div class="line mobile-hidden">
    <br><br>
      <h1 class="description">Cosa dicono di noi</h1>
  </div>



      <div class="container">

    <div class="row">
        <div class="row">
            <div class="col-md-12">
            <!-- Controls -->
            <div class="controls hidden-xs">
                    <a class="left fa fa-chevron-left btn btn-primary btn-recensioni" href="#carousel-example-generic" data-slide="prev"></a>
                        <a class="right fa fa-chevron-right btn btn-primary btn-recensioni" href="#carousel-example-generic" data-slide="next"></a>
                </div>

            </div>
        </div>
        <div id="carousel-example-generic" class="carousel slide hidden-xs" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="col-item">
                                <div class="photo">
                                    <img src="images/1.png" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-12">
                                            <h5>Giulio Grossi</h5>
                                            <h5 class="price-text-color">
                                                Ottimo servizio, buonissima la Vegetariana, a presto!</h5>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-item">
                                <div class="photo">
                                    <img src="images/2.png" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-12">
                                            <h5>
                                                Nicolas Farabegoli</h5>
                                            <h5 class="price-text-color">
                                            Ottimo servizio, buonissima la Golosa, a presto!</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-item">
                                <div class="photo">
                                    <img src="images/3.png" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-12">
                                            <h5>
                                                Igor Lirussi</h5>
                                            <h5 class="price-text-color">
                                            Ottimo servizio, buonissima la Piada Burger, a presto!</h5>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="col-item">
                                <div class="photo">
                                    <img src="images/4.png" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-12">
                                            <h5>
                                                Shapour Nemati</h5>
                                            <h5 class="price-text-color">
                                            Ottimo servizio, buonissima la Sfiziosa, a presto!</h5>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-item">
                                <div class="photo">
                                    <img src="images/5.png" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-12">
                                            <h5>
                                                Alfredo Tonelli</h5>
                                            <h5 class="price-text-color">
                                            Ottimo servizio, buonissima la Montanara, a presto!</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-item">
                                <div class="photo">
                                    <img src="images/6.png" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-12">
                                            <h5>
                                                Martina Cavallucci</h5>
                                            <h5 class="price-text-color">
                                            Ottimo servizio, buonissima la Leggera, a presto!</h5>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>










































      <div class="back-top"><a href="#" id="torna-su"><img src="images/up.png" alt="Top" id="torna-su-img"></a></div>
      <footer>
        <div class="footer" id="footer">
          <div class="container">
            <div class="row">
              <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                <h3> Chi siamo </h3>
                <ul>
                  <li> <a href="#"> Termini e Condizioni </a> </li>
                  <li> <a href="#"> Cookie Policy </a> </li>
                  <li> <a href="#dovesiamo"> Lavora con noi </a> </li>
                </ul>
              </div>
              <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                <h3> Contattaci </h3>
                <ul>
                  <li> Tel: 0547 123456</li>
                  <li> Email: infopidatogo@gmail.com</li>
                  <li> Cellulare: 333 123456</li>
                </ul>
              </div>
              <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12 ">
                <h3> Trovaci sui social </h3>
                <p>Qui potrai trovarci su diversi social, ti aspettiamo!</p>
                <ul class="social col-sm-12 col-xs-12">
                  <li> <a href="#"> <img src="images/facebook.png" alt="Facebook"> </a> </li>
                  <li> <a href="#"> <img src="images/linkedin.png" alt="LinkedIn"> </a> </li>
                  <li> <a href="#"> <img src="images/twitter.png" alt="Twitter"> </a> </li>
                  <li> <a href="#"> <img src="images/pinterest.png" alt="Pinterest"> </a> </li>
                  <li> <a href="#"> <img src="images/youtube.png" alt="YouTube"> </a> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-bottom">
          <div class="container">
            <p class="pull-left"> Copyright © Footer E-commerce Plugin 2014. All right reserved. </p>
            <div class="pull-right">
              <ul class="nav nav-pills payments col-sm-12 col-xs-12">
                <li><img src="images/p1.png" alt="Payment"></li>
                <li><img src="images/p2.png" alt="Payment"></li>
                <li><img src="images/p3.png" alt="Payment"></li>
                <li><img src="images/p4.png" alt="Payment"></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</body>
