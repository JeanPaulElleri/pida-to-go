$(document).ready(function(){
    //navbar-toggle animation
    var hide = false;
    $(".navbar-toggle").click(function(){
        $(".icon-bar").css({"transition": "all 0.2s"});

        if(!hide) {
            $(".middle-bar").hide();
            $(".top-bar").css({
                "transform-origin":"3pt 3pt",
                "transform" :"rotate(45deg)"
            });

            $(".bottom-bar").css({
                "transform-origin":"bot left",
                "transform" :"rotate(-45deg)"
            });
            hide = true;
        } else {
            $(".middle-bar").show();
            $(".top-bar").css({
                "transform" :"rotate(0deg)"
            });

            $(".bottom-bar").css({
                "transform" :"rotate(0deg)"
            });
            hide = false;
        }
    });
    //end of navbar-toggle animation


//fluid scroll navbar
    $('.navbar a[href^="#"]').on('click', function (e) {
        e.preventDefault();

        var target = this.hash,
            $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 800, 'swing', function () {
            window.location.hash = target;
        });
    });



//pulsante torna in cima dal fondo pagina fluido
    $("#torna-su-img").click(function(){
        $('html, body').animate({ scrollTop: 0 }, 900);
    });



//pulsante per mostrare e nascondere piadine
    $(".hide-me").hide();
    var flag = 0;
    $(".hideshow").click(function(){
        $(".hide-me").slideToggle("slow");

        if(flag == 0){
            $(".hideshow").css({
                "transform" :"rotate(180deg)"
            });
            flag = 1;
        } else {
            $(".hideshow").css({
                "transform" :"rotate(0deg)"
            });
            flag = 0;
        }
        return false;
    });

    $(".cart-no-id").click(function(){
      alert("Effettua il login per aggiungere piadine al carrello.");

    });

    $("div.main-img video").click(function () {
      var x = jQuery(this).offset().top + 550;
      jQuery('html,body').animate({scrollTop: x}, 500);
    });
});
