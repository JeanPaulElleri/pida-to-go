<?php

session_start();
if(isset($_SESSION["id"])){
  $id=intval($_SESSION["id"]);
}
else{
  $id=0;
}
//Dichiarazione variabili per server
$servername="localhost";
$db_username ="root";
$db_password ="";
$database = "pida_db";


$conn = new mysqli($servername, $db_username, $db_password, $database);

/***************REGISTRAZIONE***********************/
if (isset($_POST['address-submit'])){
  $indirizzo = $_POST['indirizzo'];
  $citta = $_POST['citta'];
  $cap = $_POST['cap'];
  $provincia = $_POST['provincia'];


  $sql = "UPDATE carrello SET indirizzo='$indirizzo', citta='$citta',
  cap='$cap', provincia='$provincia' WHERE id=$id";
  $result = $conn->query($sql);

  $url = "payment.php";
  header( "Location: $url" );

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pida-To-Go</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="css/custom-style.css">
  <link rel="stylesheet" href="css/ind_consegna.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script type="text/javascript" src='js/ind_consegna.js'></script>
  <script type="text/javascript" src="js/custom-js.js"></script>

</head>


<body>

  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
          <div class="navbar-header">
              <ul class="nav navbar-nav navbar-left">
                  <li><a class="navbar-brand no-padding" href="index.php"><img src="images/logo.png" alt="" id="logo"></a></li>
                  <li>
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar top-bar"></span>
                          <span class="icon-bar bottom-bar"></span>
                      </button>
                  </li>
              </ul>
          </div>

          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right center-mobile mobile-nav">
              <li class="shadow-top"><a href="index.php#proposte">Le nostre Proposte</a></li>
              <li class="shadow-top"><a href="index.php#chisiamo">Chi siamo</a></li>
              <li class="shadow-top"><a href="index.php#dovesiamo">Dove siamo</a></li>
              <?php
              if($id=='0') {
                ?>
              <li class="shadow-top"><a href="accedi_registrati.php">Accedi / Registrati</a></li>
              <?php
            } else { ?>
                <li class="shadow-top"><a href="index.php?logout=true">Logout</a></li>
              <?php }

              $sql3 = "SELECT * FROM carrello WHERE id=$id";
              $result3 = $conn->query($sql3);
              $row = $result3->fetch_assoc();

              if($row["p1"]== 0 && $row["p2"]== 0 && $row["p3"]== 0 && $row["p4"]== 0 && $row["p5"]== 0 &&
              $row["p6"]== 0 && $row["p7"]== 0 &&$row["p8"]== 0 && $row["p9"]== 0 &&
              $row["p10"]== 0 && $row["p11"]== 0 && $row["p12"]== 0 && $row["p13"]== 0 &&
              $row["p14"]== 0 && $row["p15"]== 0) { ?>
                <li><a href="cart.php?sendcode=false" id="cart-icon"><img src="images/cart28.png" alt="cart logo"></a></li>
              <?php } else { ?>
                <li><a href="cart.php?sendcode=false" id="cart-icon"><img src="images/cart-2.png" alt="cart logo"></a></li>
              <?php }
            ?>
            </ul>
          </div><!--/.nav-collapse -->
      </div>
  </nav>

  <div class="container-fluid breadcrumbBox text-center">
			<ol class="breadcrumb">
				<li>Riepilogo</li>
				<li class="active"><a>Inserisci indirizzo di consegna</a></li>
        <li>Procedi con il pagamento</li>
			</ol>
		</div>

  <div class="container">
      <div class='row'>
          <div class='col-md-4'></div>
          <div class='col-md-4'>
            <form accept-charset="UTF-8" action="#" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="pk_bQQaTxnaZlzv4FnnuZ28LFHccVSaj" role="form" id="address-form" method="post" >
              <div class='form-row'>
                <div class='col-xs-12 form-group required'>
                  <h4 class='control-label'><strong>Indirizzo</strong></h4>
                  <input class='form-control' name="indirizzo" id="indirizzo" size='4' type='text' required>
                </div>
              </div>
              <div class='form-row'>
                <div class='col-xs-12 form-group card required'>
                  <h4 class='control-label'><strong>Città</strong></h4>
                  <input autocomplete='off' class='form-control card-number' name="citta" id="citta" size='20' type='text' required>
                </div>
              </div>
              <div class='form-row'>
                <div class='col-xs-4 form-group cvc required'>
                  <h4 class='control-label'><strong>CAP</strong></h4>
                  <input autocomplete='off' class='form-control card-cvc' name="cap" id="cap" placeholder='ex. 48015' size='6' type='text' required>
                </div>
                <div class='col-xs-4 form-group cvc required'>
                  <h4 class='control-label'><strong>Provincia</strong></h4>
                  <input autocomplete='off' class='form-control card-cvc' name="provincia" id="provincia" placeholder='ex. RA' size='3' type='text' required>
                </div>
              </div>
              <div class='form-row'>
                <div class='col-md-12 form-group'>
                  <button class='form-control btn btn-default' onclick="window.location.href='cart.php?sendcode=false'">« Indietro</button>
                </div>
              </div>
              <div class='form-row'>
                <div class='col-md-12 form-group'>
                  <input class='form-control btn btn-default submit-button'  name="address-submit" id="address-submit" type='submit' value="Procedi »">
                </div>
              </div>
              <div class='form-row'>
                <div class='col-md-12 error form-group hide'>
                  <div class='alert-danger alert'>
                    Per favore, controlla di aver inserito tutti i dati e riprova.
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class='col-md-4'></div>
      </div>
  </div>
</body>
