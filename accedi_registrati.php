<?php
session_start();
if(isset($_SESSION["id"])){
  $id=intval($_SESSION["id"]);
}
else{
  $id=0;
}

//Dichiarazione variabili per server
$servername="localhost";
$db_username ="root";
$db_password ="";
$database = "pida_db";


$conn = new mysqli($servername, $db_username, $db_password, $database);

/***************REGISTRAZIONE***********************/
if (isset($_POST['register-submit'])){
  $username = $_POST['username'];
  $email = $_POST['email'];
  $password = $_POST['password'];
  $password2 = $_POST['password2'];

  //verificare username
  $query = "SELECT username FROM user WHERE username = '$username'";
  $risultato = $conn->query($query);
  if($risultato->num_rows == '0'){
    if ($password == $password2) { //verificare correttezza password
      $sql = "INSERT INTO user (username, email, password)
      VALUES('$username', '$email', '$password')";
      $result = $conn->query($sql);
    //  $_SESSION['message'] = "Ti sei loggato correttamente.";
    //  $_SESSION['username'] = $username;
      $message = "Registrazione eseguita con successo.";
      echo "<script type='text/javascript'>alert('$message');</script>";

      $query_id = "SELECT id FROM user WHERE username = '$username'";
      $result = $conn->query($query_id);
      $row = $result->fetch_assoc();
      $tmp = $row["id"];
      $sql = "INSERT INTO carrello (id)
      VALUES('$tmp')";
      $result = $conn->query($sql);

    }
    else {
      $message = "Le password non coincidono.\\nRiprova.";
      echo "<script type='text/javascript'>alert('$message');</script>";
    }
  }
  else{
    $message = "Username esistente. Riprova.";
    echo "<script type='text/javascript'>alert('$message');</script>";  }

}

/****************LOGIN*********************************************/
if (isset($_POST['login-submit'])){
  $username = $_POST['username'];
  $password = $_POST['password'];

  //verificare username
  $query = "SELECT username FROM user WHERE username = '$username'";
  $risultato = $conn->query($query);
  if($risultato->num_rows == '0'){ //username non presente
    $message = "Username non presente.";
    echo "<script type='text/javascript'>alert('$message');</script>";
  }
  else{
    $query_log = "SELECT password FROM user WHERE username = '$username' LIMIT	1";
    $result = $conn->query($query_log);
    $row = $result->fetch_assoc();
    $num = $result->num_rows;
    if ($result != '0'){

      if($row['password'] == $password){
        //login riuscito
        $message = "Login eseguito con successo.";
        echo "<script type='text/javascript'>alert('$message');</script>";
        $url = "index.php";
        header( "Location: $url" );

/****************************COSA UTILE**************************************************************/
        $query = "SELECT id FROM user WHERE username = '$username'";
        $risultato = $conn->query($query);
        $row = $risultato->fetch_assoc();
        $_SESSION["id"]=$row['id'];

      }
      else{
        //password sbagliata!
        $message = "Password sbagliata. Riprova.";
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }
  }

}


?>




<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pida-To-Go</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="css/custom-style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="js/accedi_registrati.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="custom-js.js"></script>

</head>


<body>
  <link href="css/accedi_registrati.css" rel="stylesheet" type="text/css">

  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
          <div class="navbar-header">
              <ul class="nav navbar-nav navbar-left">
                  <li><a class="navbar-brand no-padding" href="index.php"><img src="images/logo.png" alt="Pida-To-Go" id="logo"></a></li>
                  <li>
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar top-bar"></span>
                          <span class="icon-bar bottom-bar"></span>
                      </button>
                  </li>
              </ul>
          </div>

          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right center-mobile mobile-nav">
              <li class="shadow-top"><a href="index.php#proposte">Le nostre Proposte</a></li>
              <li class="shadow-top"><a href="index.php#chisiamo">Chi siamo</a></li>
              <li class="shadow-top"><a href="index.php#dovesiamo">Dove siamo</a></li>
              <li class="shadow-top"><a href="accedi_registrati.php">Accedi / Registrati</a></li>
              <?php

              $sql3 = "SELECT * FROM carrello WHERE id=$id";
              $result3 = $conn->query($sql3);
              $row = $result3->fetch_assoc();

              if($row["p1"]== 0 && $row["p2"]== 0 && $row["p3"]== 0 && $row["p4"]== 0 && $row["p5"]== 0 &&
              $row["p6"]== 0 && $row["p7"]== 0 &&$row["p8"]== 0 && $row["p9"]== 0 &&
              $row["p10"]== 0 && $row["p11"]== 0 && $row["p12"]== 0 && $row["p13"]== 0 &&
              $row["p14"]== 0 && $row["p15"]== 0) { ?>
                <li><a href="cart.php?sendcode=false" id="cart-icon"><img src="images/cart28.png" alt="cart logo"></a></li>
              <?php } else { ?>
                <li><a href="cart.php?sendcode=false" id="cart-icon"><img src="images/cart-2.png" alt="cart logo"></a></li>
              <?php }
              ?>
              </ul>
          </div><!--/.nav-collapse -->
      </div>
  </nav>

  <div class="container">
      	<div class="row">
  			<div class="col-md-6 col-md-offset-3">
  				<div class="panel panel-login">
  					<div class="panel-heading">
  						<div class="row">
  							<div class="col-xs-6">
  								<a href="#" class="active" id="login-form-link">Accedi</a>
  							</div>
  							<div class="col-xs-6">
  								<a href="#" id="register-form-link">Registrati</a>
  							</div>
  						</div>
  						<hr>
  					</div>
  					<div class="panel-body">
  						<div class="row">
  							<div class="col-lg-12">
  								<form id="login-form" action="#" method="post" role="form" style="display: block;">
  									<div class="form-group">
  										<input type="text" title="username" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" required>
  									</div>
  									<div class="form-group">
  										<input type="password" title="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>
  									</div>

  									<div class="form-group">
  										<div class="row">
  											<div class="col-sm-6 col-sm-offset-3">
  												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
  											</div>
  										</div>
  									</div>

  								</form>
  								<form id="register-form" action="#" method="post" role="form" style="display: none;">
  									<div class="form-group">
  										<input type="text" title="username" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" required>
  									</div>
  									<div class="form-group">
  										<input type="email" title="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Indirizzo Email" value="" required>
  									</div>
  									<div class="form-group">
  										<input type="password" title="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>
  									</div>
  									<div class="form-group">
  										<input type="password" title="password2" name="password2" id="password2" tabindex="2" class="form-control" placeholder="Conferma Password" required>
  									</div>
  									<div class="form-group">
  										<div class="row">
  											<div class="col-sm-6 col-sm-offset-3">
  												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Registrati Ora">
  											</div>
  										</div>
  									</div>
  								</form>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
</br>

    <footer>
      <div class="footer" id="footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
              <h3> Chi siamo </h3>
              <ul>
                <li> <a href="#"> Termini e Condizioni </a> </li>
                <li> <a href="#"> Cookie Policy </a> </li>
                <li> <a href="#dovesiamo"> Lavora con noi </a> </li>
              </ul>
            </div>
            <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
              <h3> Contattaci </h3>
              <ul>
                <li> Tel: 0547 123456</li>
                <li> Email: infopidatogo@gmail.com</li>
                <li> Cellulare: 333 123456</li>
              </ul>
            </div>
            <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12 ">
              <h3> Trovaci sui social </h3>
              <p>Qui potrai trovarci su diversi social, ti aspettiamo!</p>
              <ul class="social col-sm-12 col-xs-12">
                <li> <a href="#"> <img src="images/facebook.png" alt="Facebook"> </a> </li>
                <li> <a href="#"> <img src="images/linkedin.png" alt="LinkedIn"> </a> </li>
                <li> <a href="#"> <img src="images/twitter.png" alt="Twitter"> </a> </li>
                <li> <a href="#"> <img src="images/pinterest.png" alt="Pinterest"> </a> </li>
                <li> <a href="#"> <img src="images/youtube.png" alt="YouTube"> </a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <p class="pull-left"> Copyright © Footer E-commerce Plugin 2014. All right reserved. </p>
          <div class="pull-right">
            <ul class="nav nav-pills payments col-sm-12 col-xs-12">
              <li><img src="images/p1.png" alt="Payment"></li>
              <li><img src="images/p2.png" alt="Payment"></li>
              <li><img src="images/p3.png" alt="Payment"></li>
              <li><img src="images/p4.png" alt="Payment"></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

</body>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        <script src="../../assets/js/docs.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
